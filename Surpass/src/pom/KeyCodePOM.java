package pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import generics.BasePage;
import helpers.WaitHelpers;

public class KeyCodePOM extends BasePage
{

//	@FindBy(xpath="//input[@id='keycode-text']")
//	private WebElement keycodetextbox;
//	
//	@FindBy(xpath="//button[@id='okay']")
//	private WebElement okbutton;
	
	
	public WaitHelpers waithelper;
	
	public KeyCodePOM(WebDriver driver) {
		super(driver);
		waithelper=new WaitHelpers(driver);
	}
	
	public boolean invalidKeyCodeAttempt(String expected)
	{
		//enter code and click
		waithelper.waitForElementToBeVisible(By.xpath("//input[@id='keycode-text']"));
		driver.findElement(By.xpath("//input[@id='keycode-text']")).sendKeys("AUTOAUTO");
				
		waithelper.waitForElementToBeVisible(By.xpath("//button[@id='okay']"));
		driver.findElement(By.xpath("//button[@id='okay']")).click();
		
		waithelper.waitForElementToBeVisible(By.xpath("//p[contains(text(),'You have entered an incorrect keycode, please chec')]"));
		String actual=driver.findElement(By.xpath("//p[contains(text(),'You have entered an incorrect keycode, please chec')]")).getText();
		
		if(actual.equals(expected))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}
