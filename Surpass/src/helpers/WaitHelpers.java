package helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitHelpers 
{
	public WebDriverWait wait;
	
	public WaitHelpers(WebDriver driver)
	{
		wait=new WebDriverWait(driver, 30);
	}
	
	public void waitForElementToBeVisible(By elementBy)
	{
		wait.until(ExpectedConditions.visibilityOfElementLocated(elementBy));
	}
	
}
