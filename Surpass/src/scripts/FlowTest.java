package scripts;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import generics.BaseTest;
import pom.KeyCodePOM;

public class FlowTest extends BaseTest
{
	@Test
	public void TestFlow() throws InterruptedException
	{
		//enter code and click
		waithelper.waitForElementToBeVisible(By.xpath("//input[@id='keycode-text']"));
		driver.findElement(By.xpath("//input[@id='keycode-text']")).sendKeys("AUTO0002");
		
		waithelper.waitForElementToBeVisible(By.xpath("//button[@id='okay']"));
		driver.findElement(By.xpath("//button[@id='okay']")).click();
		
		//click confirm
		waithelper.waitForElementToBeVisible(By.xpath("//button[@id='confirm']"));
		driver.findElement(By.xpath("//button[@id='confirm']")).click();
		
		
		//select checkbox and click continue		
		waithelper.waitForElementToBeVisible(By.xpath("//span[@for='termsAcceptedCheckbox']"));
		driver.findElement(By.xpath("//span[@for='termsAcceptedCheckbox']")).click();
		
		waithelper.waitForElementToBeVisible(By.xpath("//button[@applogbutton='agreeToTerms-AcceptButton']"));
		driver.findElement(By.xpath("//button[@applogbutton='agreeToTerms-AcceptButton']")).click();
		
		//start the exam
		waithelper.waitForElementToBeVisible(By.xpath("//button[@id='startExamButton']"));Thread.sleep(3000);		
		driver.findElement(By.xpath("//button[@id='startExamButton']")).click();
		
		//question 1
		waithelper.waitForElementToBeVisible(By.xpath("//iframe[@title='Question']"));
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='Question']")));
		waithelper.waitForElementToBeVisible(By.xpath("//p[text()='Mercury']"));
		driver.findElement(By.xpath("//p[text()='Mercury']")).click();
		driver.switchTo().defaultContent();
		
		//click next
		waithelper.waitForElementToBeVisible(By.xpath("//button[@id='navToNextItemButton']"));
		driver.findElement(By.xpath("//button[@id='navToNextItemButton']")).click();
		
		//Information Page
		waithelper.waitForElementToBeVisible(By.xpath("//iframe[@title='Information Page']"));
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='Information Page']")));
		waithelper.waitForElementToBeVisible(By.xpath("//p[contains(text(),'The United Kingdom, made up of England, Scotland, ')]"));
		driver.findElement(By.xpath("//p[contains(text(),'The United Kingdom, made up of England, Scotland, ')]")).click();
		driver.switchTo().defaultContent();
		
		//click next
		waithelper.waitForElementToBeVisible(By.xpath("//button[@id='navToNextItemButton']"));
		driver.findElement(By.xpath("//button[@id='navToNextItemButton']")).click();
		
		//Question 2
		waithelper.waitForElementToBeVisible(By.xpath("//iframe[@title='Question']"));
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='Question']")));
		waithelper.waitForElementToBeVisible(By.xpath("//p[text()='Malaysia']"));
		driver.findElement(By.xpath("//p[text()='Malaysia']")).click();
		waithelper.waitForElementToBeVisible(By.xpath("//p[text()='India']"));
		driver.findElement(By.xpath("//p[text()='India']")).click();		
		driver.switchTo().defaultContent();
		
		//click next
		waithelper.waitForElementToBeVisible(By.xpath("//button[@id='navToNextItemButton']"));
		driver.findElement(By.xpath("//button[@id='navToNextItemButton']")).click();
		
		//Information Page
		waithelper.waitForElementToBeVisible(By.xpath("//iframe[@title='Information Page']"));
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='Information Page']")));
		waithelper.waitForElementToBeVisible(By.xpath("//p[contains(text(),'Italy, a European country with a long Mediterranea')]"));
		driver.findElement(By.xpath("//p[contains(text(),'Italy, a European country with a long Mediterranea')]")).click();
		driver.switchTo().defaultContent();
				
		//click next
		waithelper.waitForElementToBeVisible(By.xpath("//button[@id='navToNextItemButton']"));
		driver.findElement(By.xpath("//button[@id='navToNextItemButton']")).click();
		
		//Question 3
		waithelper.waitForElementToBeVisible(By.xpath("//iframe[@title='Question']"));
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='Question']")));
		waithelper.waitForElementToBeVisible(By.xpath("//p[text()='False']"));
		driver.findElement(By.xpath("//p[text()='False']")).click();	
		driver.switchTo().defaultContent();
				
		//click next
		waithelper.waitForElementToBeVisible(By.xpath("//button[@id='navToNextItemButton']"));
		driver.findElement(By.xpath("//button[@id='navToNextItemButton']")).click();
		
		//Information Page
		waithelper.waitForElementToBeVisible(By.xpath("//iframe[@title='Information Page']"));
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='Information Page']")));
		waithelper.waitForElementToBeVisible(By.xpath("//b[contains(text(),'India ')]"));
		driver.findElement(By.xpath("//b[contains(text(),'India ')]")).click();
		driver.switchTo().defaultContent();
		
		
		//click Finish exam button
		waithelper.waitForElementToBeVisible(By.xpath("//button[@id='finishExamButton']"));
		driver.findElement(By.xpath("//button[@id='finishExamButton']")).click();
		
		//click Finish button
		waithelper.waitForElementToBeVisible(By.xpath("//button[@id='details-incorrect']"));
		driver.findElement(By.xpath("//button[@id='details-incorrect']")).click();
				
		//click Finish button
		waithelper.waitForElementToBeVisible(By.xpath("//button[@id='details-return']"));
		driver.findElement(By.xpath("//button[@id='details-return']")).click();
		
	}
	
	@Test
	public void invalidKeyCodetest()
	{
		KeyCodePOM keycodeobject=new KeyCodePOM(driver);
		boolean haveKeycode=keycodeobject.invalidKeyCodeAttempt("You have entered an incorrect keycode, please check and try again.");
		Assert.assertEquals(haveKeycode, true);
		
	}
	
}
