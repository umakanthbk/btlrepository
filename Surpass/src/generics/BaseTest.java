package generics;

import java.io.IOException;
import java.net.MalformedURLException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import helpers.ScreenshotHelper;
import helpers.WaitHelpers;

import java.net.URL;
import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseTest 
{
	public WebDriver driver;
	
	public WaitHelpers waithelper;
	
	@BeforeMethod
	public void openAppln()
	{
		WebDriverManager.chromedriver().setup();
		driver=new ChromeDriver();
		driver.get("https://regression.surpass-preview.com/secureassess/htmldelivery/?isFake=true#!#%2Fkeycode");
		waithelper=new WaitHelpers(driver);
		driver.manage().window().maximize();
	}
	
//	public static final String USERNAME = "umakanth_oaMpwD";
//	public static final String AUTOMATE_KEY = "H9JsWEsHZ9aD8mxQyWWA";
//	public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
//	@BeforeMethod
//	public void openApplninBrowserStack() throws MalformedURLException
//	{
//		DesiredCapabilities caps = new DesiredCapabilities();
//		caps.setCapability("os", "Windows");
//		caps.setCapability("os_version", "10");
//		caps.setCapability("browser", "Chrome");
//		caps.setCapability("browser_version", "latest");
//		caps.setCapability("browserstack.local", "false");
//		caps.setCapability("browserstack.selenium_version", "3.14.0");
//		driver = new RemoteWebDriver(new URL(URL), caps);		
//		driver.get("https://regression.surpass-preview.com/secureassess/htmldelivery/?isFake=true#!#%2Fkeycode");
//		waithelper=new WaitHelpers(driver);
//		driver.manage().window().maximize();
//	}
	
	@AfterMethod
	public void closeAppln(ITestResult result) throws IOException
	{
		String testMethodName=result.getMethod().getMethodName();
		System.out.println("Method Name is: "+testMethodName);
		if(!result.isSuccess())
		{
			ScreenshotHelper screenshotobject=new ScreenshotHelper();
			screenshotobject.captureScreenshot(driver, testMethodName);
		}
		driver.quit();
	}
}
